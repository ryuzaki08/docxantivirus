import zipfile
import xml.etree.ElementTree as ET
import shutil
import pandas as pd
import scipy.stats as ss
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.feature_selection import SelectKBest, mutual_info_classif
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.model_selection import GridSearchCV
from lightgbm import LGBMClassifier

def parseXML(root, sm, lst):
    sm = sm + "/" + root.tag[root.tag.rfind('}') + 1:]
    for child in root:
        parseXML(child, sm, lst)
    lst.append(sm)

def file_to_paths(file, path):
    struct_path = []
    l=zipfile.ZipFile(file, 'r')
    l_list=l.namelist()
    temp_folder = path + '/Temp'
    l.extractall(temp_folder)
    for elem in l_list:
        if(elem.find('.xml') != -1):
            tree = ET.parse(temp_folder + '/' + elem)
            root = tree.getroot()
            parseXML(root, elem, struct_path)
        else:
            struct_path.append(elem)
    shutil.rmtree(temp_folder)
    return struct_path

class Preprocessor(BaseEstimator, TransformerMixin):
    def __init__(self, vocabulary):
        self.vocabulary = vocabulary
    
    def fit(self, documents, labels):
        self.vectorizer = TfidfVectorizer(token_pattern='\S+', vocabulary=self.vocabulary)
        X = self.vectorizer.fit_transform(documents)
        X = pd.DataFrame(X.todense())
        self.selector = SelectKBest(mutual_info_classif, k=50)
        self.selector.fit(X, labels)
        return self
    
    def transform(self, documents):
        X = self.vectorizer.transform(documents)
        X = pd.DataFrame(X.todense())
        return pd.DataFrame(self.selector.transform(X))
    
    def fit_transform(self, documents, labels):
        self.fit(documents, labels)
        return self.transform(documents)

class DOCXClassifier(BaseEstimator, ClassifierMixin, TransformerMixin):
    def __init__(self):
        pass
    
    def fit(self, X, y):
        rf = RandomForestClassifier(bootstrap=True, random_state=42)
        lgb = LGBMClassifier(is_unbalance=True, random_state=42)
        svm = SVC()
        rf_param_grid = {'n_estimators': [100, 150, 200], 'max_depth': [10, 20, 30, 50]}
        lgb_param_grid = {'num_leaves': [31, 51, 71, 101], 'max_depth': [10, 20, 30, 50],
                         'learning_rate': [0.1, 0.01, 0.001], 'n_estimators': [50, 100, 150]}
        svm_param_grid = {'C': [0.1, 0.5, 1, 5, 10, 15], 'gamma': [0.1, 0.5, 1, 5, 10, 15]}
        self.rf_grid = GridSearchCV(rf, rf_param_grid, scoring='recall')
        self.lgb_grid = GridSearchCV(lgb, lgb_param_grid, scoring='recall')
        self.svm_grid = GridSearchCV(svm, svm_param_grid, scoring='recall')
        self.rf_grid.fit(X, y)
        self.lgb_grid.fit(X, y)
        self.svm_grid.fit(X, y)
        return self
        
    def predict(self, X):
        pred1 = self.rf_grid.predict(X)
        pred2 = self.lgb_grid.predict(X)
        pred3 = self.svm_grid.predict(X)
        preds = []
        for i in range(len(pred1)):
            preds.append(ss.mode([pred1[i], pred2[i], pred3[i]])[0][0])
        return preds