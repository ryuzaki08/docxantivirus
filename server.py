import streamlit as st
import zipfile
import pickle
from functions import file_to_paths
from functions import Preprocessor
from functions import DOCXClassifier


st.write("""
# Malicious DOCX Prediction App
This app predicts whether your docx-file benign or malicious
""")

document_tfidf = []

def button_clicked():
    if uploaded_file is None:
        return
    elif not zipfile.is_zipfile(uploaded_file):
        st.write('Uploaded file must be a docx!')
        return
    else:
        try:
            document_tfidf.append(' '.join(file_to_paths(uploaded_file, 'samples')))
        except zipfile.BadZipFile as e:
            st.write('Couldn\'t parse. Probably the file is damaged.')
            return
    with open('estimator.pk', 'rb') as file:
	    clf = pickle.load(file)

    with open('preprocessor.pk', 'rb') as file:
	    prep = pickle.load(file)

    X = prep.transform(document_tfidf)
    y_pred = clf.predict(X)
    labels = ['benign', 'malicious']
    st.subheader('Prediction')
    st.write(f'Your file is {labels[y_pred[0]]}')
    
uploaded_file = st.file_uploader(label='Choose a docx-file', type=['docx',])
button = st.button(label='Check your file', on_click=button_clicked)