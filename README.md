<h1>DOCX File Antivirus</h1>
This application checks whether your docx-file malicious or not.

It uses machine learning techniques.

To launch the application activate your python environment, install necessary dependencies, go to the project's directory in command line (use `cd` command) and write `streamlit run server.py`.

All the necessary libs can be found in _requirements.txt_.

Initial research with model's performance on train and validation data can be found in _Course_project.ipynb_.

If you want to run _train.py_ to retrain the model, you'll need train data for it. Benign files are already stored to `ProjectFolder/samples/benign` You'll have to unarchive malicious _.zip_ files from _malware_ folder to `ProjectFolder/samples/malicious`. Password is "_infected_".

**DO NOT EXECUTE THEM!**
