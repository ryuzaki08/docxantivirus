import os
import zipfile
import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from functions import file_to_paths
from functions import Preprocessor
from functions import DOCXClassifier

LEGAL_PATH = "samples/benign"
VIRUS_PATH = "samples/malicious"
benign_filenames = [LEGAL_PATH + '/' + x for x in os.listdir(path=LEGAL_PATH)]
virus_filenames = [VIRUS_PATH + '/' + x for x in os.listdir(path=VIRUS_PATH)]

documents_tfidf = []
labels = []
all_paths = []

for file in benign_filenames:
    if zipfile.is_zipfile(file):
        p = file_to_paths(file, LEGAL_PATH)
        documents_tfidf.append(' '.join(p))
        all_paths.extend(p)
        labels.append(0)
for file in virus_filenames:
    if zipfile.is_zipfile(file):
        try:
            p = file_to_paths(file, VIRUS_PATH)
            documents_tfidf.append(' '.join(p))
            all_paths.extend(p)
            labels.append(1)
        except:
            continue

all_paths = list(set(all_paths))

prep = Preprocessor(all_paths)
X = prep.fit_transform(documents_tfidf, labels)

X_train, X_test, y_train, y_test = train_test_split(X, labels,
                                                    stratify=labels, 
                                                    test_size=0.3, random_state=42)



clf = DOCXClassifier()
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))
print(pd.DataFrame(confusion_matrix(y_test, y_pred)))

with open('estimator.pk', 'wb') as file:
	pickle.dump(clf, file)

with open('preprocessor.pk', 'wb') as file:
	pickle.dump(prep, file)